//
//  AppNavigationController.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 03/09/2021.
//

import UIKit

class AppNavigationController: UINavigationController {
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isNavigationBarHidden = true
        self.navigationBar.isTranslucent = false
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
}
