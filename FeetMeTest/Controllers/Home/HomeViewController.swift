//
//  HomeViewController.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 03/09/2021.
//

import UIKit
import SnapKit

class HomeViewController: UIViewController {
    
    var flowDelegate: AppFlow?
    
    private var logoImageView = UIImageView(image: UIImage(named: "logo"))
    private var leftFootImage = UIImageView(image: UIImage(named: "left_foot"))
    private var rightFootImage = UIImageView(image: UIImage(named: "right_foot"))
    
    // Test Values
    private var timer: Timer?
    private var currentInsole: InsoleType = .left
    private var tmpLeftFrames = [String]()
    private var tmpRightFrames = [String]()
    private var totalDistance: Float = 0.0
    private var totalDuration: UInt16 = 0
    private var totalVelocity: Float = 0.0
    
    private var startButton: UIButton = {
        let button = UIButton()
        button.setTitle("Start test", for: .normal)
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 27.0
        return button
    }()
    
    private var leftDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Verdana", size: 14.0)
        return label
    }()
    
    private var rightDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Verdana", size: 14.0)
        return label
    }()
    
    private var distanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        label.alpha = 0.0
        label.text = "Distance parcourue:"
        label.font = UIFont(name: "Verdana", size: 20.0)
        return label
    }()
    
    var showStartButton: Bool = true {
        didSet {
            if oldValue != self.showStartButton {
                UIView.animate(withDuration: 0.3) {
                    self.startButton.alpha = self.showStartButton ? 1.0 : 0.0
                    self.distanceLabel.alpha = self.showStartButton ? 0.0 : 1.0
                }
            }
        }
    }
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.logoImageView)
        self.logoImageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(100.0)
            make.width.equalTo(100.0)
            make.top.equalTo(60.0)
        }
        
        // Image foots animation
        self.leftFootImage.alpha = 0.0
        self.view.addSubview(self.leftFootImage)
        self.leftFootImage.snp.makeConstraints { make in
            make.top.equalTo(self.logoImageView.snp.bottom).offset(80.0)
            make.height.width.equalTo(120.0)
            make.left.equalTo(self.view.snp.left).offset(Screen.size.width * 0.1)
        }
        
        self.rightFootImage.alpha = 0.0
        self.view.addSubview(self.rightFootImage)
        self.rightFootImage.snp.makeConstraints { make in
            make.top.equalTo(self.logoImageView.snp.bottom).offset(80.0)
            make.height.width.equalTo(120.0)
            make.right.equalTo(self.view.snp.right).offset(-(Screen.size.width * 0.1))
        }
        
        // Display data
        self.view.addSubview(self.leftDataLabel)
        self.leftDataLabel.snp.makeConstraints { make in
            make.top.equalTo(self.leftFootImage.snp.bottom).offset(30.0)
            make.width.equalTo(120.0)
            make.centerX.equalTo(self.leftFootImage.snp.centerX)
        }
        
        self.view.addSubview(self.rightDataLabel)
        self.rightDataLabel.snp.makeConstraints { make in
            make.top.equalTo(self.rightFootImage.snp.bottom).offset(30.0)
            make.width.equalTo(120.0)
            make.centerX.equalTo(self.rightFootImage.snp.centerX)
        }
        
        self.view.addSubview(self.distanceLabel)
        self.distanceLabel.snp.makeConstraints { make in
            make.top.equalTo(self.leftDataLabel.snp.bottom).offset(70.0)
            make.width.equalTo(150.0)
            make.centerX.equalToSuperview()
        }
        
        // Start Button
        self.startButton.addTarget(self, action: #selector(start), for: .touchUpInside)
        self.view.addSubview(self.startButton)
        self.startButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view.snp.bottom).offset(-30.0 - self.view.safeAreaInsets.bottom)
            make.left.equalTo(self.view.snp.left).offset(Screen.size.width * 0.1)
            make.right.equalTo(self.view.snp.right).offset(-(Screen.size.width * 0.1))
            make.height.equalTo(54.0)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        // Reinit the view for a new test
        self.showStartButton = true
        self.showLeftInsole(false)
        self.showRightInsole(false)
        self.totalVelocity = 0
        self.totalDuration = 0
        self.totalDistance = 0
        self.distanceLabel.text = ""
    }

    
    //MARK: - Actions
    @objc func start() {
        self.showStartButton = false
        
        // Set up tmp values
        guard let left = self.flowDelegate?.left?.frames,
              let right = self.flowDelegate?.right?.frames else {
            return
        }
        
        self.tmpLeftFrames = left
        self.tmpRightFrames = right
        
        // Start timer 1seconde
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(displayInsoles), userInfo: nil, repeats: true)
    }
    
    @objc func displayInsoles() {
        
        guard !self.tmpRightFrames.isEmpty && !self.tmpLeftFrames.isEmpty else {
            // End of loop
            self.endTest()
            
            return
        }
        
        if self.currentInsole == .left {
            guard let data = self.tmpLeftFrames.first,
                  let velocity = data.extractVelocity(),
                  let length = data.extractLenght(),
                  let duration = data.extractDuration() else {
                return
            }
            
            self.leftDataLabel.text = "Vitesse: \n \(String(describing: velocity)) m/s"
            self.showLeftInsole(true)
            self.showRightInsole(false)
            
            self.tmpLeftFrames.removeFirst()
            self.currentInsole = .right
            
            self.totalDistance += length.round(to: 2)
            self.totalDuration += duration
            self.totalVelocity += velocity
            
            self.distanceLabel.text = "Distance parcourue: \n \(self.totalDistance)m"
        }
        else {
            guard let data = self.tmpRightFrames.first,
                  let velocity = data.extractVelocity(),
                  let length = data.extractLenght(),
                  let duration = data.extractDuration() else {
                return
            }
            
            self.rightDataLabel.text = "Vitesse: \n \(String(describing: velocity)) m/s"
            self.showLeftInsole(false)
            self.showRightInsole(true)
            
            self.tmpRightFrames.removeFirst()
            self.currentInsole = .left
            
            self.totalDistance += length.round(to: 2)
            self.totalDuration += duration
            self.totalVelocity += velocity
            
            self.distanceLabel.text = "Distance parcourue: \n \(self.totalDistance) m"
        }
    }
    
    func endTest() {
        // Stop timer
        self.timer?.invalidate()
        self.timer = nil
        
        guard let leftCount = self.flowDelegate?.left?.frames.count,
              let rightCount = self.flowDelegate?.right?.frames.count else {
            return
        }
        
        let totalFrames = leftCount + rightCount
        
        // Continue to result VC
        self.flowDelegate?.continueToResult(self, distance: self.totalDistance, velocity: Float(self.totalVelocity) / Float(totalFrames), duration: self.totalDuration / UInt16(totalFrames))
    }
    
    
    // MARK: - Animations
    func showLeftInsole(_ show: Bool = false) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: []) {
            self.leftFootImage.alpha = show ? 1.0 : 0.0
            self.leftDataLabel.alpha = show ? 1.0 : 0.0
        } completion: { _ in
            
        }
    }
    
    func showRightInsole(_ show: Bool = false) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: []) {
            self.rightFootImage.alpha = show ? 1.0 : 0.0
            self.rightDataLabel.alpha = show ? 1.0 : 0.0
        } completion: { _ in
            
        }
    }
}
