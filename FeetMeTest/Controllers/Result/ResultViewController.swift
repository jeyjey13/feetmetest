//
//  ResultViewController.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 06/09/2021.
//

import UIKit

class ResultViewController: UIViewController {
    
    var flowDelegate: AppFlow?

    private var distanceCardView = ResultCardview()
    private var velocityCardView = ResultCardview()
    private var durationCardView = ResultCardview()
    private var distance: Float
    private var velocity: Float
    private var duration: UInt16
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont(name: "Verdana", size: 24.0)
        label.text = "Résultat"
        return label
    }()
    
    private var finishButton: UIButton = {
        let button = UIButton()
        button.setTitle("Ok", for: .normal)
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 27.0
        
        return button
    }()
    
    
    // MARK: - Life cycle
    init(duration: UInt16, distance: Float, velocity: Float) {
        self.duration = duration
        self.distance = distance
        self.velocity = velocity
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.view.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(30.0)
            make.width.equalTo(200.0)
            make.top.equalTo(60.0)
        }
        
        self.distanceCardView.feedView(title: "Distance totale", detail: String(self.distance.round(to: 2)) + " m")
        self.view.addSubview(self.distanceCardView)
        self.distanceCardView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(120.0)
            make.width.equalTo(280.0)
        }
        
        self.durationCardView.feedView(title: "Temps moyen", detail: String(self.duration) + " ms")
        self.view.addSubview(self.durationCardView)
        self.durationCardView.snp.makeConstraints { make in
            make.height.equalTo(120.0)
            make.width.equalTo(280.0)
            make.bottom.equalTo(self.distanceCardView.snp.top).offset(-50.0)
            make.centerX.equalToSuperview()
        }
        
        self.velocityCardView.feedView(title: "Vitesse moyenne", detail: String(self.velocity.round(to: 2)) + " m/s")
        self.view.addSubview(self.velocityCardView)
        self.velocityCardView.snp.makeConstraints { make in
            make.height.equalTo(120.0)
            make.width.equalTo(280.0)
            make.top.equalTo(self.distanceCardView.snp.bottom).offset(50.0)
            make.centerX.equalToSuperview()
        }
        
        // Finish Button
        self.finishButton.addTarget(self, action: #selector(finish), for: .touchUpInside)
        self.view.addSubview(self.finishButton)
        self.finishButton.snp.makeConstraints { make in
            make.bottom.equalTo(self.view.snp.bottom).offset(-30.0 - self.view.safeAreaInsets.bottom)
            make.left.equalTo(self.view.snp.left).offset(Screen.size.width * 0.1)
            make.right.equalTo(self.view.snp.right).offset(-(Screen.size.width * 0.1))
            make.height.equalTo(54.0)
        }
    }
    
    
    //MARK: - Actions
    @objc func finish() {
        self.flowDelegate?.backAction()
    }
}
