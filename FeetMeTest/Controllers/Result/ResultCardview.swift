//
//  ResultCardview.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 06/09/2021.
//

import UIKit

class ResultCardview: UIView {
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "Verdana", size: 16.0)
        return label
    }()
    
    private var detailLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "Verdana", size: 30.0)
        return label
    }()
    
    
    // MARK: - Life cycle
    init() {
        super.init(frame: .zero)
        
        self.configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - View
    func configureView() {
        self.backgroundColor = .black
        self.layer.cornerRadius = 20.0
        
        self.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30.0)
            make.width.equalToSuperview()
            make.top.equalTo(self.snp.top).offset(10.0)
        }
        
        self.addSubview(self.detailLabel)
        self.detailLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30.0)
            make.width.equalToSuperview()
            make.top.equalTo(self.titleLabel.snp.bottom).offset(15.0)
        }
    }

    func feedView(title: String, detail: String) {
        self.titleLabel.text = title
        self.detailLabel.text = detail
    }
}
