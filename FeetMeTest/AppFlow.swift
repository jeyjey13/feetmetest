//
//  AppFlow.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 03/09/2021.
//

import UIKit
import CoreBluetooth

class AppFlow: Flow {
    
    var navigation: AppNavigationController!
    var left: Insole?
    var right: Insole?
    
    init() {
        self.left = Insole.loadInsole(file: .left)
        self.right = Insole.loadInsole(file: .right)
    }
    
    func start() -> UIViewController {
        let homeController = HomeViewController()
        homeController.flowDelegate = self
        self.navigation = AppNavigationController(rootViewController: homeController)
        
        return self.navigation
    }
}

protocol AppFlowDelegate {
    // ContinueTo
    func continueToResult(_ controller: UIViewController, distance: Float, velocity: Float, duration: UInt16)
    
    // Back action
    func backAction()
}


extension AppFlow: AppFlowDelegate {
    // Continue to
    func continueToResult(_ controller: UIViewController, distance: Float, velocity: Float, duration: UInt16) {
        let resultController = ResultViewController(duration: duration, distance: distance, velocity: velocity)
        
        resultController.flowDelegate = self
        self.navigation.pushViewController(resultController, animated: true)
    }
    
    func backAction() {
        self.navigation.popViewController(animated: true)
    }
}
