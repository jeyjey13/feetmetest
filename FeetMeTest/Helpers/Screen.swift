//
//  Screen.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 05/09/2021.
//

import UIKit

public struct Screen {

    public static var size: CGSize {
        return UIScreen.main.bounds.size
    }

    public static var pixelSize: CGSize {
        let screenSize = UIScreen.main.bounds.size
        let scale = UIScreen.main.scale

        return CGSize(width: screenSize.width * scale,
            height: screenSize.height * scale)
    }
}
