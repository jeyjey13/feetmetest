//
//  Flow.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 03/09/2021.
//

import UIKit.UIViewController

protocol Flow {
    func start() -> UIViewController
}
