//
//  myExtension.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 04/09/2021.
//

import UIKit

let hexadecimalRegex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)

extension String {
    var hexadecimal: Data? {
        var data = Data(capacity: count / 2)
        
        hexadecimalRegex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }
    
    func extractLenght() -> Float? {
        guard let data = self.hexadecimal, data.count > 0 else {
            return nil
        }
        
        return data.subdata(in: 0..<4).floatValue().round(to: 2)
    }
    
    func extractDuration() -> UInt16? {
        guard let data = self.hexadecimal, data.count > 0 else {
            return nil
        }
        
        return data.subdata(in: 4..<6).intValue()
    }
    
    func extractVelocity() -> Float? {
        guard let data = self.hexadecimal, data.count > 0 else {
            return nil
        }
        
        return data.subdata(in: 6..<10).floatValue()
    }
}


extension Data {
    func floatValue() -> Float {
        return Float(bitPattern: UInt32(littleEndian: self.withUnsafeBytes { $0.load(as: UInt32.self) }))
    }
    
    func intValue() -> UInt16 {
        return UInt16(littleEndian: self.withUnsafeBytes { $0.load(as: UInt16.self) })
    }
}

extension Float {
    func round(to places: Int = 2) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
