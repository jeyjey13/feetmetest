//
//  Insole.swift
//  FeetMeTest
//
//  Created by Jeremy Gros on 05/09/2021.
//

import UIKit

enum InsoleType: String {
    case left = "left"
    case right = "right"
}

enum SerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}

struct Insole {
    var frames = [String]()
    var metadata: (size: String, side: String, protocol: Int)
    
    init(json: [String: Any]) throws {
        
        // Extract and validate metadata
        guard let metadataJSON = json["metadata"] as? [String: Any],
              let size = metadataJSON["insoleSize"] as? String,
              let side = metadataJSON["insoleSide"] as? String,
              let protocole = metadataJSON["insoleProtocol"] as? Int
        else {
            throw SerializationError.missing("metadata")
        }
        
        let metadata = (size, side, protocole)
        
        // Extract and validate frames
        guard let framesJSON = json["frames"] as? Array<Any> else {
            throw SerializationError.missing("frames")
        }

        var frames = [String]()
        for frame in framesJSON {
            if let frameString = frame as? [String:String],
               let bytes = frameString["bytes"] {
                frames.append(bytes)
            }
        }
        
        // Initialize properties
        self.frames = frames
        self.metadata = metadata
    }
    
    
    //MARK: -  Download
    static func loadInsole(file: InsoleType) -> Insole? {
        guard let path = Bundle.main.path(forResource: file.rawValue + "_insole", ofType: "json") else {
            print("Error to find file path...")
            return nil
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            //load Data from Json file
            let data = try Data(contentsOf: url)
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            
            // Create Insole
            // How support 2 protocols ?
            // Check the protocol from the json
            // Call 2 differents Init from Insole Model for each protocol
            guard let insoleJson = json else { return nil }
            let insole = try Insole(json: insoleJson)
            
            return insole
        }
        catch {
            print("🛑 \(error)")
            return nil
        }
    }
}
